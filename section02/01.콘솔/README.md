# Qt6 프로그래밍의 시작

## 01.Console 상에 Hello Worlld를 출력하는 예제 프로그램 구현

### 프로젝트 생성
```
1. Choose a template
  Application(Qt) :Qt Console Application

2. Project Location 
  Name: Console_Application

3. Define Build System
  qmake

4. Translation File
  Language: <none>

5. Kit Selection
  Desktop Qt x.x.x MinGW 64-bit
  Debug

6. Project Managerment
  version control: <None>
```

![console-app-0000](./images/console-app-0000.png)

![console-app-0001](./images/console-app-0001.png)

![console-app-0002](./images/console-app-0002.png)

![console-app-0003](./images/console-app-0003.png)

![console-app-0004](./images/console-app-0004.png)

![console-app-0005](./images/console-app-0005.png)

![console-app-0006](./images/console-app-0006.png)

![console-app-0007](./images/console-app-0007.png)


### 빌드 & 실행
* 빌드
![console-app-0008](./images/console-app-0008.png)

* 실행
![console-app-0009](./images/console-app-0009.png)
> NOTE. 3 Application Output 창에서 확인 가능

* 콘솔창을 띄워서 테스트 하고자 하는 경우
![console-app-0009](./images/console-app-0010.png)


___
.END OF PROGRAMMING

