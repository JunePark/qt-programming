# 기본 데이터 타입과 유용한 타입들

## 02.QBitArray와 QByteArray

![QBitArray-QByteArray-0000](./images/QBitArray-QByteArray-0000.png)

![QBitArray-QByteArray-0001](./images/QBitArray-QByteArray-0001.png)

![QBitArray-QByteArray-0002](./images/QBitArray-QByteArray-0002.png)

![QBitArray-QByteArray-0003](./images/QBitArray-QByteArray-0003.png)

![QBitArray-QByteArray-0004](./images/QBitArray-QByteArray-0004.png)

![QBitArray-QByteArray-0005](./images/QBitArray-QByteArray-0005.png)

![QBitArray-QByteArray-0006](./images/QBitArray-QByteArray-0006.png)

![QBitArray-QByteArray-0007](./images/QBitArray-QByteArray-0007.png)

![QBitArray-QByteArray-0008](./images/QBitArray-QByteArray-0008.png)


___
.END OF DATATYPE

