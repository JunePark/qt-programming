# Qt6 프로그래밍의 시작

## 03.Designer를 사용해 GUI 프로그램 구현

### 프로젝트 생성
```
1. Choose a template
  Application(Qt) :Qt Widget Application

2. Project Location 
  Name: GUI_app

3. Define Build System
  qmake

4. Class Information
  Base class : QWidget
  Generate form : check on

5. Translation File
  Language: <none>

6. Kit Selection
  Desktop Qt x.x.x MinGW 64-bit
  Debug

7. Project Managerment
  version control: <None>
```

![designer-app-0000](./images/designer-app-0000.png)

![designer-app-0001](./images/designer-app-0001.png)

![designer-app-0002](./images/designer-app-0002.png)

![designer-app-0003](./images/designer-app-0003.png)

![designer-app-0004](./images/designer-app-0004.png)

![designer-app-0005](./images/designer-app-0005.png)

![designer-app-0006](./images/designer-app-0006.png)


### 구현
![designer-app-0007](./images/designer-app-0007.png)

![designer-app-0008](./images/designer-app-0008.png)

![designer-app-0009](./images/designer-app-0009.png)

![designer-app-0010](./images/designer-app-0010.png)

![designer-app-0011](./images/designer-app-0011.png)

![designer-app-0012](./images/designer-app-0012.png)

![designer-app-0013](./images/designer-app-0013.png)

![designer-app-0014](./images/designer-app-0014.png)

![designer-app-0015](./images/designer-app-0015.png)


___
.END OF PROGRAMMING

