# Signal 과 Slot

## 01.Signal 과 Slot의 이해

![signal-slot-0000](./images/signal-slot-0000.png)

![signal-slot-0001](./images/signal-slot-0001.png)

![signal-slot-0002](./images/signal-slot-0002.png)

![signal-slot-0003](./images/signal-slot-0003.png)


## 02.Signal 과 Slot을 연결하는 방법과 Signal 과 Signal 을 연결하기

![signal-slot-0004](./images/signal-slot-0004.png)

![signal-slot-0005](./images/signal-slot-0005.png)
___
.END OF PROGRAMMING

