# GUI 프로그래밍

## 03.QList Container 클래스와 Structure 를 이용한 예제 구현

### 프로젝트 생성
```
1. Choose a template
  Application(Qt) :Qt Widget Application

2. Project Location 
  Name: Emp_Example

3. Define Build System
  qmake

4. Class Information
  Base class : QWidget
  Generate form : check on

5. Translation File
  Language: <none>

6. Kit Selection
  Desktop Qt x.x.x MinGW 64-bit
  Debug

7. Project Managerment
  version control: <None>
```

### 구현
![Emp_Example-0000](./images/Emp_Example-0000.png)



___
.END OF EMP_EXAMPLE

