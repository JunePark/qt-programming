# GUI 프로그래밍 - Widgets

## 03.QMdiArea Widget을 이용한 다중 윈도우 창을 만들기 위한 예제 구현

### 프로젝트 생성
```
1. Choose a template
  Application(Qt) :Qt Widget Application

2. Project Location 
  Name: MDI_area_example

3. Define Build System
  qmake

4. Class Information
  Base class : QWidget
  Generate form : check off

5. Translation File
  Language: <none>

6. Kit Selection
  Desktop Qt x.x.x MinGW 64-bit
  Debug

7. Project Managerment
  version control: <None>
```

### 구현
![QMdiArea-window-0000](./images/QMdiArea-window-0000.png)

![QMdiArea-window-0001](./images/QMdiArea-window-0001.png)

![QMdiArea-window-0002](./images/QMdiArea-window-0002.png)

___
.END OF WIDGET

