# 기본 데이터 타입과 유용한 타입들

[01.Qt에서 제공하는 데이터타입의 종류및 특징](01.Qt에서 제공하는 데이터타입의 종류및 특징/README.md)

[02.QBitArray와 QByteArray](02.QBitArray와 QByteArray/README.md)

[03.QString](03.QString/README.md)

[04.QVariant, QPoint, QRect 그리고 QRegion](04.QVariant, QPoint, QRect 그리고 QRegion/README.md)


___
.END OF DATATYPE

