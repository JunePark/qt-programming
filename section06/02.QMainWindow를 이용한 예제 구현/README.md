# GUI 프로그래밍 - QMainWindow

## 02.QMainWindow를 이용한 예제 구현

### 프로젝트 생성
```
1. Choose a template
  Application(Qt) :Qt Widget Application

2. Project Location 
  Name: 01_QMainWindow

3. Define Build System
  qmake

4. Class Information
  Base class : QMainWindow
  Generate form : check off

5. Translation File
  Language: <none>

6. Kit Selection
  Desktop Qt x.x.x MinGW 64-bit
  Debug

7. Project Managerment
  version control: <None>
```

### 구현
![QMainWindow-example-0000](./images/QMainWindow-example-0000.png)

![QMainWindow-example-0001](./images/QMainWindow-example-0001.png)

![QMainWindow-example-0002](./images/QMainWindow-example-0002.png)

![QMainWindow-example-0003](./images/QMainWindow-example-0003.png)

![QMainWindow-example-0004](./images/QMainWindow-example-0004.png)

![QMainWindow-example-0005](./images/QMainWindow-example-0005.png)

![QMainWindow-example-0006](./images/QMainWindow-example-0006.png)

![QMainWindow-example-0007](./images/QMainWindow-example-0007.png)

![QMainWindow-example-0008](./images/QMainWindow-example-0008.png)

![QMainWindow-example-0009](./images/QMainWindow-example-0009.png)

![QMainWindow-example-0010](./images/QMainWindow-example-0010.png)

![QMainWindow-example-0011](./images/QMainWindow-example-0011.png)

![QMainWindow-example-0012](./images/QMainWindow-example-0012.png)


___
.END OF QMAINWINDOW

