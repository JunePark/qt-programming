# Signal 과 Slot

## 02.Signal 과 Slot을 열견하는 예제 구현

### 프로젝트 생성
```
1. Choose a template
  Application(Qt) :Qt Widget Application

2. Project Location 
  Name: signalslot_exam

3. Define Build System
  qmake

4. Class Information
  Base class : QWidget
  Generate form : check on

5. Translation File
  Language: <none>

6. Kit Selection
  Desktop Qt x.x.x MinGW 64-bit
  Debug

7. Project Managerment
  version control: <None>
```

![ignalslot-exam-0000](./images/signalslot-exam-0000.png)

![ignalslot-exam-0001](./images/signalslot-exam-0001.png)

![ignalslot-exam-0002](./images/signalslot-exam-0002.png)

![ignalslot-exam-0003](./images/signalslot-exam-0003.png)

![ignalslot-exam-0004](./images/signalslot-exam-0004.png)

![ignalslot-exam-0005](./images/signalslot-exam-0005.png)

![ignalslot-exam-0006](./images/signalslot-exam-0006.png)

### 구현
![ignalslot-exam-0007](./images/signalslot-exam-0007.png)

![ignalslot-exam-0008](./images/signalslot-exam-0008.png)
> CTL+S 로 저장후 닫기

![ignalslot-exam-0009](./images/signalslot-exam-0009.png)

![ignalslot-exam-0010](./images/signalslot-exam-0010.png)

![ignalslot-exam-0011](./images/signalslot-exam-0011.png)


___
.END OF PROGRAMMING

