# GUI 프로그래밍 - Layouts

## 02.Layout을 이용한 예제 구현

### 프로젝트 생성
```
1. Choose a template
  Application(Qt) :Qt Widget Application

2. Project Location 
  Name: 01_Layout

3. Define Build System
  qmake

4. Class Information
  Base class : QWidget
  Generate form : check off

5. Translation File
  Language: <none>

6. Kit Selection
  Desktop Qt x.x.x MinGW 64-bit
  Debug

7. Project Managerment
  version control: <None>
```

### 구현
![layout-example-0000](./images/layout-example-0000.png)

![layout-example-0001](./images/layout-example-0001.png)

![layout-example-0002](./images/layout-example-0002.png)


___
.END OF LAYOUTS

