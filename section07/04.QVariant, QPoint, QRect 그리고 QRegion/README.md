# 기본 데이터 타입과 유용한 타입들

## 04.QVariant, QPoint, QRect 그리고 QRegion

![QVariant-etc-0000](./images/QVariant-etc-0000.png)

![QVariant-etc-0001](./images/QVariant-etc-0001.png)

![QVariant-etc-0002](./images/QVariant-etc-0002.png)

![QVariant-etc-0003](./images/QVariant-etc-0003.png)


___
.END OF DATATYPE

