# Model View

## 03.Model_View 예제를 이용해 사용방법 익히기

![Model_View-Example-0000](./images/Model_View-Example-0000.png)

![Model_View-Example-0001](./images/Model_View-Example-0001.png)

![Model_View-Example-0002](./images/Model_View-Example-0002.png)

![Model_View-Example-0003](./images/Model_View-Example-0003.png)

![Model_View-Example-0004](./images/Model_View-Example-0004.png)

![Model_View-Example-0005](./images/Model_View-Example-0005.png)

![Model_View-Example-0006](./images/Model_View-Example-0006.png)



___
.END OF MODEL_VIEW

