# Qt6 프로그래밍의 시작

## 02.Designer를 사용하지 않고 GUI 프로그램 구현

### 프로젝트 생성
```
1. Choose a template
  Application(Qt) :Qt Widget Application

2. Project Location 
  Name: GUI_Example

3. Define Build System
  qmake

4. Class Information
  Base class : QWidget
  Generate form : check off

5. Translation File
  Language: <none>

6. Kit Selection
  Desktop Qt x.x.x MinGW 64-bit
  Debug

7. Project Managerment
  version control: <None>
```

![gui-app-0000](./images/gui-app-0000.png)

![gui-app-0001](./images/gui-app-0001.png)

![gui-app-0002](./images/gui-app-0002.png)

![gui-app-0003](./images/gui-app-0003.png)

![gui-app-0004](./images/gui-app-0004.png)

![gui-app-0005](./images/gui-app-0005.png)

![gui-app-0006](./images/gui-app-0006.png)

### 구현
![gui-app-0007](./images/gui-app-0007.png)

![gui-app-0008](./images/gui-app-0008.png)

![gui-app-0009](./images/gui-app-0009.png)
> CTL+E 3(우측에 편집창), CTL+E 2(아래쪽에 편집창)

![gui-app-0010](./images/gui-app-0010.png)

___
.END OF PROGRAMMING

