# GUI 프로그래밍 - Widgets

## Widgets

[01.Widget들의 종류와 특징](01.Widget들의 종류와 특징/README.md)

[02.자주사용되는 Widget들](02.자주사용되는 Widget들/README.md)

[03.QMdiArea Widget을 이용한 다중 윈도우 창을 만들기 위한 예제 구현](03.QMdiArea Widget을 이용한 다중 윈도우 창을 만들기 위한 예제 구현/README.md)
___
.END OF WIDGET

