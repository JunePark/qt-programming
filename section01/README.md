# Qt6 설치

## Windows 11 기준

![qt6-install-0000](./images/qt6-install-0000.png)

![qt6-install-0001](./images/qt6-install-0001.png)

![qt6-install-0002](./images/qt6-install-0002.png)

![qt6-install-0003](./images/qt6-install-0003.png)

![qt6-install-0004](./images/qt6-install-0004.png)

![qt6-install-0005](./images/qt6-install-0005.png)

![qt6-install-0006](./images/qt6-install-0006.png)

![qt6-install-0007](./images/qt6-install-0007.png)

![qt6-install-0008](./images/qt6-install-0008.png)

![qt6-install-0009](./images/qt6-install-0009.png)

![qt6-install-0010](./images/qt6-install-0010.png)

![qt6-install-0011](./images/qt6-install-0011.png)

![qt6-install-0012](./images/qt6-install-0012.png)

![qt6-install-0013](./images/qt6-install-0013.png)

![qt6-install-0014](./images/qt6-install-0014.png)

___
.END OF INSTALLATION

